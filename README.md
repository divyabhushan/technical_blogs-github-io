# List of my published blogs

I am a Content Developer for technical articles. I constantly like to learn and share and grow my knowledge. 
Developing technical material in an easy to understand way is my passion.

This is a collection of my Articles, Tutorials and blogs written by me so far... 
=======

1. [Docker Vs Virtual Machines(VMs)](https://www.knowledgehut.com/blog/devops/docker-vs-vm)
2. [DevOps Interview Questions](https://www.knowledgehut.com/interview-questions/devops-questions)
3. [Git: Basic terms and commands explained](https://dzone.com/articles/git-basic-terminologies-explained)
4. [Git Reflog – How to recover a deleted branch that was not merged](https://www.edureka.co/blog/git-reflog/)
5. [Git Tutorial](https://www.knowledgehut.com/tutorials/git-tutorial)
6. [How To Secure Your Git Project Using an Easy Branching Strategy](https://dzone.com/articles/git-branching-structural-strategy)
7. [This is how you share your work on a git remote repository](https://www.edureka.co/blog/this-is-how-you-share-your-work-on-a-git-remote-repository/)
8. [DevOps Roadmap to Become a Successful DevOps Engineer](https://www.knowledgehut.com/blog/devops/devops-roadmap)
9. [Git bisect: How to identify a bug in your code?](https://www.edureka.co/blog/git-bisect/)
10. [How to use Git Log to format the commit history?](https://www.edureka.co/blog/git-format-commit-history/)
11. [What are the common Git mistakes and how to fix them?](https://www.edureka.co/blog/common-git-mistakes/)
12. [How to get Data Science Jobs](https://in.springboard.com/blog/how-to-get-data-science-jobs/)
13. [What is a Data Scientist? Who should take up Data Science?](https://in.springboard.com/blog/what-is-data-scientist/)
14. [How to Make a Career Transition from Software Developer to Data Scientist](https://in.springboard.com/blog/software-developer-to-data-scientist/)
15. [Artificial Intelligence vs Human Intelligence: Humans, not machines, will build the future](https://in.springboard.com/blog/artificial-intelligence-vs-human-intelligence/?utm_source=Facebook&utm_medium=Social&utm_campaign=AI-Vs-Humans-Facebook)
16. [Women in STEM: How Data Science and AI/ML are Opening up New Career Opportunities for Women](https://in.springboard.com/blog/women-in-stem/)

=======

